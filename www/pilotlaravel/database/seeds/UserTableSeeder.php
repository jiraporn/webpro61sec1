<?php
/**
 * Created by PhpStorm.
 * User: iclassroom
 * Date: 27/11/2018 AD
 * Time: 18:59
 */
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        \Illuminate\Foundation\Auth\User::create(array(
            'name'     => 'jiraporn',
            'username' => 'mint',
            'email'    => 'jiraporn@msu.ac.th',
            'password' => Hash::make('kiml19997'),
        ));
    }

}
