<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()//สร้างดาต้าเบส
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',256)->charset('utf8');
            $table->string("category",256)->charset("utf8");
            $table->text("description")->charset("utf8");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            //
        });
    }
}
