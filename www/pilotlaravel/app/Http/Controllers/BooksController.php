<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BooksController extends Controller
{
   /* function store(Requeast $request)
    {
       $book = new Books();
       $book->setAttribute("name",$request->name);
       $book->setAttribute("category",$request->category);
       $book->setAttribute("description",$request->description);
       if ($book->save()){
           return true;
       }
    }*/

    function store(Request $request){
        $book = new Books();
        if(Books::created($request->all())){
            return true;
        }
    }

    function update(Request $request, Books $book){
        if ($book->fill($request->all())){
            return true;
        }
    }

    function index(){
        $books = Books::all();
        return $books;
    }

    function show(Books $books){
        return $books;
    }

    function destroy(Books $books){
        if ($books->delete()){
            return true;
        }
    }

}
